# b2csv

_b2csv_ is a utility that searches on Bexio for contacts, matches the related contacts, and prints them as CSV to stdout.

## Example

```bash
$ b2csv -C ABCD123 -U 1 -P abcd123 -S abcd123 export -k contact_group_ids -v 1 > result.csv
$ cat result.csv
first_name,last_name,company_name,address,zip_city
Hans,Wurscht,nxt Engineering GmbH,Regina-Kägi-Strasse 11,8050 Zürich
```

All available search parameters can bee seen in the [Bexio API documentation](https://docs.bexio.com/resources/contact/).
In the _Search Contacts_ section, click on _Search Parameters_.

If either `-k` or `-v` is not defined or empty, all contacts are getting exported.

## Commands

The `export` command:

```
NAME:
   b2csv export - Exports the matching contacts to a CSV.

USAGE:
   b2csv export [command options] [arguments...]

OPTIONS:
   --FilterKey value, -k value    The key to filter the contacts
   --FilterValue value, -v value  The value to filter the contacts (default: 0)
```

Run `b2csv` or `b2csv help` to see also recently introduced commands:

```
NAME:
   b2csv - Import outlook contacts csv to bexio

USAGE:
   b2csv [global options] command [command options] [arguments...]

VERSION:
   0.1.0

AUTHOR:
   Christian Mäder <christian.maeder@nxt.engineering>

COMMANDS:
   export, e  Exports the matching contacts to a CSV.
   help, h    Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --CompanyID value, -C value     The Company ID as assigned by Bexio
   --UserID value, -U value        The User ID as assigned by Bexio (default: 0)
   --PublicKey value, -P value     The Public Key as assigned by Bexio
   --SignatureKey value, -S value  The Signature Key as assigned by Bexio
   --help, -h                      show help
   --version, -v                   print the version

COPYRIGHT:
   (c) 2019-2021 nxt Engineering GmbH
```

## Installation

The following command should install _b2csv_ on your computer.
You need to have a recent version of Go installed on your computer for this to work.
Also make sure that the `$GOPATH` part of your `$PATH`.

```bash
git clone https://gitlab.com/nxt/public/b2csv.git
cd b2csv
go install
``` 

You will also need an API key for your Bexio instance.
You can get it on the following page:
https://office.bexio.com/index.php/client_settings/apiKey_list
