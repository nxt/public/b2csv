package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gocarina/gocsv"
	"github.com/urfave/cli"

	"gitlab.com/nxt/public/bexio"
)

type Contact struct {
	FirstName   string `csv:"first_name"`
	LastName    string `csv:"last_name"`
	CompanyName string `csv:"company_name"`
	Address     string `csv:"address"`
	ZIPCity     string `csv:"zip_city"`
}

func main() {
	log.SetOutput(os.Stderr)

	app := cli.NewApp()
	app.Version = "0.1.0"
	app.Name = "b2csv"
	app.Usage = "Import outlook contacts csv to bexio"
	app.Author = "Christian Mäder <christian.maeder@nxt.engineering>"
	app.Copyright = "(c) 2019-2021 nxt Engineering GmbH"
	app.EnableBashCompletion = true

	flags := []cli.Flag{
		cli.StringFlag{
			Name:     "CompanyID, C",
			Usage:    "The Company ID as assigned by Bexio",
			Required: true,
		},
		cli.IntFlag{
			Name:     "UserID, U",
			Usage:    "The User ID as assigned by Bexio",
			Required: true,
		},
		cli.StringFlag{
			Name:     "PublicKey, P",
			Usage:    "The Public Key as assigned by Bexio",
			Required: true,
		},
		cli.StringFlag{
			Name:     "SignatureKey, S",
			Usage:    "The Signature Key as assigned by Bexio",
			Required: true,
		},
	}
	app.Flags = flags

	app.Commands = []cli.Command{
		{
			Name:    "export",
			Aliases: []string{"e"},
			Usage:   "Exports the matching contacts to a CSV.",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:     "FilterKey, k",
					Usage:    "The key to filter the contacts",
					Required: false,
				},
				cli.IntFlag{
					Name:     "FilterValue, v",
					Usage:    "The value to filter the contacts",
					Required: false,
				},
			},
			Action: func(c *cli.Context) error {
				return ExportContacts(c, createBexioClient(c))
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func createBexioClient(c *cli.Context) *bexio.Client {
	client := bexio.NewClient(buildAPIToken(c))

	return &client
}

func buildAPIToken(c *cli.Context) bexio.APIToken {
	return bexio.APIToken{
		CompanyID:    c.GlobalString("CompanyID"),
		UserID:       c.GlobalInt("UserID"),
		PublicKey:    c.GlobalString("PublicKey"),
		SignatureKey: c.GlobalString("SignatureKey"),
	}
}

func ExportContacts(ctx *cli.Context, client *bexio.Client) error {
	key := ctx.String("FilterKey")
	value := ctx.String("FilterValue")

	var err error
	var contactsInFilter []bexio.Contact

	if key == "" || value == "" {
		contactsInFilter, err = client.ListContacts()
	} else {
		contactsInFilter, err = client.SearchContacts(map[string]interface{}{key: []interface{}{value}})
	}

	if err != nil {
		return err
	}

	contactToContactMap := make(map[bexio.Contact]bexio.Contact, len(contactsInFilter))

	for _, contact := range contactsInFilter {
		if contact.ContactType == bexio.ContactTypePerson {
			relations, err := client.SearchContactRelation(map[string]interface{}{"contact_id": contact.ID})

			if err != nil {
				log.Printf("Failed to get relations for contact %d (%s %s): %s", contact.ID, contact.FirstName, contact.LastName, err)
				continue
			}

			relations2, err := client.SearchContactRelation(map[string]interface{}{"contact_sub_id": contact.ID})

			if err != nil {
				log.Printf("Failed to get relations2 for contact %d (%s %s): %s", contact.ID, contact.FirstName, contact.LastName, err)
				continue
			}

			totalRelations := len(relations) + len(relations2)
			if totalRelations != 1 {
				log.Printf("%d relations for contact %d (%s %s), skipping the contact.", totalRelations, contact.ID, contact.FirstName, contact.LastName)
				continue
			}

			var relatedContactId int
			if len(relations) == 1 {
				relatedContactId = relations[0].ContactSubID
			} else {
				relatedContactId = relations2[0].ContactID
			}

			relatedContact, err := client.ShowContact(relatedContactId)

			if err != nil {
				log.Printf("Failed to get related contact for id %d of contact %d (%s %s): %s", relatedContactId, contact.ID, contact.FirstName, contact.LastName, err)
				continue
			}

			contactToContactMap[contact] = relatedContact
		}
	}

	contacts := make([]Contact, len(contactToContactMap))
	i := 0
	for contact, relatedContact := range contactToContactMap {
		contacts[i] = Contact{
			FirstName:   contact.FirstName,
			LastName:    contact.LastName,
			CompanyName: relatedContact.LastName,
			Address:     relatedContact.Address,
			ZIPCity:     fmt.Sprintf("%s %s", relatedContact.Postcode, relatedContact.City),
		}

		i++
	}

	csvContent, err := gocsv.MarshalString(&contacts)

	if err != nil {
		return err
	}

	fmt.Println(csvContent)
	return nil
}
